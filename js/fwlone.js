﻿Pace.on('hide', function(){
	$('#fw-container').show();
	$('.pace').style('display', 'none!important');
});
	
jQuery('document').ready(function(){		
	jQuery('.fw-navbar a').on('click', function(e){
		e.preventDefault();
		$('body').fadeOut();
		setTimeout(getContent, 1500, $(this).attr('fw-url'));
	});
	
	$(window).mousemove(function(e) {
		var xpos=e.clientX;
		var ypos=e.clientY;
		
		var top = (-10+(ypos/100));
		if(top > 0){ top = 0; }else if(top < -10){ top = -10; }
		
		var left = (-10+(xpos/100));
		if(left > 0){ left = 0; }else if(left < -10){ left = -10; }
		
		$('.fw-slide img').css('margin-top', top+'vh');
		$('.fw-slide img').css('margin-left', left+'%');				   
	});
	
	$('.fw-left-column').fullpage({
		navigation:true,
	});
	
});

window.addEventListener("popstate", function(e) {
	getContent(location.pathname, false);
});

function getContent(url) {
	$.get(url).done(function( data ) {

		const regex = /<div id="fw-container">(.*)<\/div><!-- Конец контейнера -->/gms;
		content = regex.exec(data);
		content = content[1].replace('</div><!-- Конец контейнера -->', '');
		
		$('#fw-container').html(content);
		history.pushState(null, null, url);
		$('body').addClass('notpace');
		$.fn.fullpage.destroy('all');
		$('.fw-left-column').fullpage({
			navigation:true,
		});
		
		$('body').fadeIn();
	});
}